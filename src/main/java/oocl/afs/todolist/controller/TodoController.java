package oocl.afs.todolist.controller;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    TodoResponse create(@RequestBody TodoCreateRequest todoCreateRequest){
        final Todo todo = TodoMapper.toEntity(todoCreateRequest);
        return todoService.createTodo(todo);
    }

    @PutMapping("/{todoId}")
    TodoResponse update(@PathVariable Long todoId, @RequestBody TodoCreateRequest todoCreateRequest){
        return todoService.updateTodo(todoId, todoCreateRequest);
    }

    @DeleteMapping("/{todoId}")
    void delete(@PathVariable Long todoId){
        todoService.deleteTodo(todoId);
    }

}
