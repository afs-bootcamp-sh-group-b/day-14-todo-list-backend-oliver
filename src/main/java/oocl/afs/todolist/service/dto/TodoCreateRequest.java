package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {

    private String name;
    private Boolean done;

    public TodoCreateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String text) {
        this.name = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
