package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoEtity -> TodoMapper.toResponse(todoEtity))
                .collect(Collectors.toList());
    }

    public TodoResponse createTodo(Todo todo) {
        final Todo todoEntity = todoRepository.save(todo);
        return TodoMapper.toResponse(todoEntity);
    }

    public TodoResponse updateTodo(Long todoId, TodoCreateRequest todoRequest) {
        Todo existingTodo = todoRepository.findById(todoId).orElse(null);
        if (existingTodo != null) {
            existingTodo.setName(todoRequest.getName());
            existingTodo.setDone(todoRequest.getDone());

            Todo updatedTodo = todoRepository.save(existingTodo);
            return TodoMapper.toResponse(updatedTodo);
        }
        return null;
    }

    public void deleteTodo(Long todoId){
        todoRepository.deleteById(todoId);
    }
}
