package oocl.afs.todolist.service.mapper;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    private TodoMapper() {
    }
    public static Todo toEntity(TodoCreateRequest request) {
        return new Todo(null, request.getName(), false);
    }

    public static TodoResponse toResponse(Todo todoEntity) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todoEntity, todoResponse);
        return todoResponse;
    }
}
