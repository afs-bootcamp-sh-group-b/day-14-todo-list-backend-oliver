package oocl.afs.todolist;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;
    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }
    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todoRepository.save(todo);

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()));
    }

    @Test
    void should_create_todo() throws Exception{
        String json = "{\"name\":\"oliver\"}";
        mockMvc.perform(post("/todo").content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Long id = todoRepository.findAll().get(0).getId();

        final boolean success = todoRepository.findById(id).stream()
                .allMatch(todo -> todo.getName().equals("oliver"));
        assertTrue(success);
    }

    @Test
    void should_put_done_todo() throws Exception{
        Todo todo = todoRepository.save(new Todo(1L, "code review", false));
        String json = "{\"id\":1,\"done\":true}";
        mockMvc.perform(put("/todo/" + todo.getId())
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        final boolean success = todoRepository.findById(todo.getId()).stream()
                .allMatch(todoItem -> todoItem.getDone().equals(true));
        assertTrue(success);
    }

    @Test
    void should_put_text_todo() throws Exception{
        Todo todo = todoRepository.save(new Todo(1L, "code review", false));
        String json = "{\"id\":1,\"name\":\"haha\"}";
        mockMvc.perform(put("/todo/" + todo.getId())
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        final boolean success = todoRepository.findById(todo.getId()).stream()
                .allMatch(todoItem -> todoItem.getName().equals("haha"));
        assertTrue(success);
    }

    @Test
    void should_delete_todo() throws Exception{
        Todo todo = todoRepository.save(new Todo(1L, "code review", false));
        mockMvc.perform(delete("/todo/" + todo.getId()))
                .andExpect(status().isOk());

        final boolean success = todoRepository.findAll().stream()
                .noneMatch(todoItem -> todoItem.getId().equals(todo.getId()));
        assertTrue(success);
    }

}
