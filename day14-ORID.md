## day14

### O:
今天上午学习了前后端的集成，如何解决跨域问题。
下午做了elevator speech, mvp, user story, user journey的分享，
并进行最后项目的构想

### R:
感到兴奋和担忧，兴奋在于下一周即将对整个训练营的知识进行实践，担忧在于自己的
知识储备不够，会遇到很多困难。

### I:
通过在后端添加信任的请求源就可以解决跨域的问题。
在介绍自己、产品、服务...时，可以用elevator speech的原则进行，抓住重点，吸引对象的眼球。

### D:
希望自己可以在接下来的一周中加油，和团队成员一起完成最终的项目！